//
//  MKBottomSheetViewControllerPresenter.swift
//  MKViewControllerPresenter
//
//  Created by Rolf Koczorek on 01.09.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

class MKBottomSheetViewControllerPresenter: MKViewControllerPresenter, UIGestureRecognizerDelegate {
   
    var height: CGFloat = 200
    var tapGestureRecognizer: UITapGestureRecognizer?
    
    override func present(destinationViewController: UIViewController) {
        super.present(destinationViewController)
    }
    
    override func setupContainer() {
        super.setupContainer()
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapRecognized:")
        tapGestureRecognizer!.delegate = self
        container!.addGestureRecognizer(tapGestureRecognizer!)
    }
    
    func tapRecognized(tapRecognizer: UITapGestureRecognizer) {
        dismiss()
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer!, shouldReceiveTouch touch: UITouch!) -> Bool {
        return !CGRectContainsPoint(presentedView!.frame, touch.locationInView(container!))
    }
    
    override func show(completion: () -> ()) {
        presentedView!.frame = destinationViewControllerFrameDisplaced()
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
            self.presentedView!.frame = self.destinationViewControllerFrame()
            }, completion: { completed in
                completion()
        })
    }
    
    override func hide(completion: () -> ()) {
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
            self.presentedView!.frame = self.destinationViewControllerFrameDisplaced()
            }, completion: { completed in
                completion()
        })
    }
    
    func destinationViewControllerFrame() -> CGRect {
        return CGRectMake(0, container!.frame.size.height - _size().height, _size().width, _size().height)
    }
    
    func destinationViewControllerFrameDisplaced() -> CGRect {
        var frame = destinationViewControllerFrame()
        frame.origin = CGPointMake(frame.origin.x, container!.frame.height)
        return frame
    }
    
    override func _size() -> CGSize {
        return CGSizeMake(container!.frame.size.width, height)
    }
    
}
