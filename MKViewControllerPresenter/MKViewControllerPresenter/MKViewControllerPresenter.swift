//
//  MKViewControllerPresenter.swift
//  Segue
//
//  Created by Matteo Koczorek on 31.08.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

@objc protocol MKViewControllerPresenterDelegate {
    optional func viewControllerPresenter(viewControllerPresenter:MKViewControllerPresenter, didDismissViewController viewController:UIViewController)
    optional func viewControllerPresenter(viewControllerPresenter:MKViewControllerPresenter, didPresentViewController viewController:UIViewController)
    optional func viewControllerPresenter(ViewControllerPresenter:MKViewControllerPresenter, willDismissViewController viewController: UIViewController)
}

class MKViewControllerPresenter: NSObject {
    
    var size: CGSize?
    var presentingViewController: UIViewController?
    var presentedViewController: UIViewController? {
        didSet {
            presentedView = presentedViewController?.view
        }
    }
    var delegate: MKViewControllerPresenterDelegate?
    var container: UIView?
    var presentedView: UIView?
    
    init(presentingViewController:UIViewController) {
        super.init()
        self.presentingViewController = presentingViewController
    }
    
    func present(presentedViewController: UIViewController) {
        
        self.presentedViewController = presentedViewController
        
        if(presentingViewController == nil) {
            return
        }
        
        setupContainer()
        addPresentedViewController()
        
        show({
            //completed
            self.didShow()
        });
    }
    
    func setupContainer() {
        container = UIView()
        applyContainerFrame()
        presentingViewController!.view.addSubview(container!)
    }
    
    func applyContainerFrame() {
        container!.frame.size = presentingViewController!.view.frame.size
    }
    
    func addPresentedViewController() {
        presentedView!.frame.size = _size()
        presentingViewController!.addChildViewController(presentedViewController!)
        container!.addSubview(presentedViewController!.view)
        presentedViewController!.didMoveToParentViewController(presentingViewController!)
        
    }
    
    func dismiss() {
        self.delegate?.viewControllerPresenter?(self, willDismissViewController: self.presentedViewController!)
        hide({
            self.container!.removeFromSuperview()
            self.container = nil
            self.presentedViewController!.removeFromParentViewController()
            self.didHide()
            self.presentedViewController = nil
        })
    }

    func show(completion:()->()) {
       //override for custom animation
        completion()
        
    }
    
    
    func hide(completion:()->()) {
       //override for custom animation
        completion()
    }
    
    //dedicated delegate call methods since delegate optional methods do not seem to work inside blocks at this time (XCode 6 - beta 4)
    func didShow() {
        self.delegate?.viewControllerPresenter?(self, didPresentViewController: self.presentedViewController!)
    }
    
    func didHide() {
        self.delegate?.viewControllerPresenter?(self, didPresentViewController: self.presentedViewController!)
    }
    
    func _size() -> CGSize {
        if(size == nil) {
            return container!.frame.size
        }
        return size!
    }
    
    
    
}
