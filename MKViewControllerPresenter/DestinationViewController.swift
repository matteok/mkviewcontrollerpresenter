//
//  DestinationViewController.swift
//  MKViewControllerPresenter
//
//  Created by Rolf Koczorek on 01.09.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

protocol DestinationViewControllerDelegate {
    func destinationViewControllerDidPressClose(destinationViewController:DestinationViewController)
}

class DestinationViewController: UIViewController {

    var delegate: DestinationViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonPressed(sender: AnyObject) {
        self.delegate?.destinationViewControllerDidPressClose(self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
