//
//  ViewController.swift
//  MKViewControllerPresenter
//
//  Created by Rolf Koczorek on 31.08.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DestinationViewControllerDelegate {
    
    var presenter: MKViewControllerPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonPressed(sender: UIButton) {
        switch(sender.tag) {
        case 0:
            presenter = MKViewControllerPresenter(presentingViewController: self)
            break
        case 1:
            presenter = MKSheetViewControllerPresenter(presentingViewController: self)
            presenter!.size = CGSizeMake(200, 300)
            break
        case 2:
            presenter = MKBlurredModalPresenter(presentingViewController: self)
            break
        case 3:
            presenter = MKBottomSheetViewControllerPresenter(presentingViewController: self)
            break
        case 4:
            presenter = MKCustomRectViewControllerPresenter(presentingViewController: self)
            (presenter! as MKCustomRectViewControllerPresenter).rect = CGRect(x: 50, y: 100, width: 400, height: 300);
            break
        default:
            presenter = MKViewControllerPresenter(presentingViewController: self)
            break
        }
        var vc = self.storyboard!.instantiateViewControllerWithIdentifier("destinationVC") as DestinationViewController
        vc.delegate = self
        presenter!.present(vc)
        
    }
    
    func destinationViewControllerDidPressClose(destinationViewController: DestinationViewController) {
        presenter!.dismiss()
    }


}

